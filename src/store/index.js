import Vue from 'vue'
import Vuex from 'vuex'
import { getItem, setItem } from '@/utils/storeage.js'

Vue.use(Vuex)

const TOUTIAO_USER = 'TOKEN_KEY'

export default new Vuex.Store({
  state: {
    user: getItem(TOUTIAO_USER)
  },
  mutations: {
    setUser (state, user) {
      state.user = user
      setItem(TOUTIAO_USER, state.user)
    }
  },
  actions: {},
  modules: {}
})
