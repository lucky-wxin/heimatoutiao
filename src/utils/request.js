/**
 * 封装 axios 请求模块
 */
import axios from 'axios'
import store from '@/store'

const request = axios.create({
  baseURL: 'http://toutiao.itheima.net' // 基础路径
})

request.interceptors.request.use(
  function (config) {
    const user = store.state.user
    if (user?.token) {
      config.headers.Authorization = `Bearer ${user.token}`
    }
    return config
  },
  err => {
    return Promise.reject(err)
  }
)

export default request
