import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 引入Vant全部组件
import Vant from 'vant'
import 'vant/lib/index.css'

// 引入rem适配文件
import 'amfe-flexible'
// 引入全局样式表
import '@/styles/index.less'

// 引入时间格式化文件
import './utils/dayjs'

// 注册全部vant组件
Vue.use(Vant)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
