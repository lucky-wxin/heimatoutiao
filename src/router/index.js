import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// 路由配置路径表
const routes = [
  {
    path: '/login',
    name: 'login',
    // 路由懒加载 目的是 将每个路由页面集成为一个路由 在需要这个页面加载时 才会请求这个路由
    component: () => import('@/views/login/index.vue')
  },
  {
    path: '/',
    // name: 'layout',
    component: () => import('@/views/layout/index.vue'),
    children: [
      {
        path: '/',
        name: 'home',
        component: () => import('@/views/home/index.vue')
      },
      {
        path: 'qa',
        name: 'qa',
        component: () => import('@/views/qa/index.vue')
      },
      {
        path: 'video',
        name: 'video',
        component: () => import('@/views/video/index.vue')
      },
      {
        path: 'my',
        name: 'my',
        component: () => import('@/views/my/index.vue')
      }
    ]
  },
  {
    path: '/search',
    name: 'search',
    component: () => import('@/views/search/index.vue')
  },
  // 文章详情路由
  {
    path: '/article/:articleId',
    name: 'article',
    component: () => import('@/views/article/index.vue'),
    // props 可以把路由路径中的参数传给组件
    props: true
  },
  // 编辑资料
  {
    path: '/user/profile',
    name: 'userProfile',
    component: () => import('@/views/userProfile/index.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
